package SeleniumSessions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsConcepts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\usa\\Desktop\\seleniumDrivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("https://www.toolsqa.com/automation-practice-form/");
		
		
		// FindElement -> method. it is available in WebDriver
		// By -> class
		// xpath(), id() -> method
		
		// what action you want to perform -> click(), clear(), sendKeys()
		
		/*
		 * Total 8 Locators
		 * 
		 * Xpath :- 
		 * 
		 */
		driver.findElement(By.xpath("//*[@name='firstname']")).sendKeys("test");

		driver.findElement(By.xpath("//*[@name='lastname']")).sendKeys("test last");
		
		
		// ID
		// Name
		// LinkText
		// partial LinkText - not Useful
		/*
		* CSS Selector - immportant one
		* if id is abvailable use #adderess, class name .address	
		*/
			
		
		// Class - Not that much in use. Class name can be duplicate
		
	}

}
