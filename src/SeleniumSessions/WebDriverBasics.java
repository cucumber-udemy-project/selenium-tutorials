package SeleniumSessions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverBasics {

	public static void main(String[] args) {
		
		/*
		 * Launch FF driver ( GECKO Driver)
		 *
		 */ 
		
		// gecko dirver is a class to use it you have to create a object.
		// To launch gecko we have ff driver class. we just need to create an object of firefox
		
		// Sysyem is a CLASS
		// setProperty is method
		
		
		//System.setProperty("webdriver.gecko.driver", "C:\\Users\\usa\\Desktop\\seleniumDrivers\\geckodriver.exe");
		
		/*
		 *    / => forward Slash
		 *    If you are using windows you need to write .EXE in mac exe doesn't work.
		 * 		WebDriver is an interface 
		*/
		
		//  WebDriver   driver = new FirefoxDriver(); // Launch the Browser.
		//interface 					// class
		
		//driver.get("https://www.google.com");
		
		// ff driver is implementing  Webdriver interface.
		
		
		
		/*   ==== 2  =====
		 * Launch Chrome browser 
		 *
		 */ 
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\usa\\Desktop\\seleniumDrivers\\chromedriver.exe");
		
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com");
		
		
		// To Get the title of the page 
		
		 String title = driver.getTitle();
		 
		 // To compare the String 
		 // validating => Actual vs Expected 
		 if(title.equals("Google")) {
			 System.out.println("correct Ttitle");
		 }
		 else {
			 System.out.println("Incorrect Title");
		 }
		 
		 // GET CURRENT URL
		 driver.getCurrentUrl();

		 // GET PAGE SOURCE 
		 driver.getPageSource();
		 
		 
		 // To QUIT the browser 
		 driver.quit();
	}

}
